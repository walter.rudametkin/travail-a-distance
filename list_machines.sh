#!/bin/sh
MORE_MACHINES=""

if [ $# -eq 1 ]; then
    if [ $1 = "-h" ]; then
        echo "Usage: list_machines.sh <options>"
        echo "Options are:"
        echo "\t-h\t Print this message"
        echo "\t-f\t Scan ALL machines. This may take a long time!"
        exit 0
    elif [ $1 = "-f" ]; then
            MORE_MACHINES="reuze bimberlot phinaert clodion florine astruc gedeon"
    fi
fi
    
for machine in gambrinus lyderic gayant $MORE_MACHINES; do  
  for i in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 ; do  
      ping -c1 -W1 $machine$i 2> /dev/null > /dev/null && echo $machine$i is alive  
  done  
done  
